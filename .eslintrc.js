export default {
    "env": {
        "es2021": true
    },

    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 12,
        "sourceType": "module"
    },
    "rules": {
    },
    settings: {
        'import/resolver': {
            node: {
                extensions: ['.js', '.jsx'],
                moduleDirectory: ['node_modules', 'src'],
            },
        },
    },
};
