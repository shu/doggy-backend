export default {
   getAll: async (opts, db) => {
      const data = await db.raw(`
SELECT a.*,
       ai.image_url imageUrl
FROM   animal a
       LEFT JOIN (SELECT *
                  FROM   animal_image
                  WHERE  id IN (SELECT Min(id)
                                FROM   animal_image
                                GROUP  BY animal_id)) AS ai
              ON a.id = ai.animal_id 
        `)
      return data[0]
   },
   getOneById: async (opt, db) => {
      const data = await db.raw(`
SELECT
   a.*,
   GROUP_CONCAT(ai.image_url SEPARATOR ', ') imageUrl 
FROM
   animal a 
   LEFT JOIN
      animal_image ai 
      ON a.id = ai.animal_id 
where
   a.id = ?
        
        `, [opt.id])
      return data[0][0]
   },
   add: async (opts, db) => {

      const data = await db.raw(`
INSERT INTO animal
            (status,
             name,           
             description)
VALUES      (?,
             ?,           
             ?) 
        `, [opts.status, opts.name, opts.description])
      return data[0].insertId
   },
   addImage: async (animalId, db) => {

      const data = await db.raw(`
INSERT INTO animal_image
            (animal_id)
            VALUES(?)
        `, [animalId])
      return data[0].insertId
   },
   updateImageById: async (imageUrl, id, db) => {
      const data = await db.raw(`
UPDATE animal_image
SET    image_url = ?
WHERE  id = ? 
        `, [imageUrl, id])
      return data[0]
   },
   getLastImageId: async (db) => {
      const data = await db.raw(`
SELECT Max(id) AS id
FROM   animal_image 
        `)
      return data[0][0].id
   },
   addImages: async (insertArray, db) => {

      const data = await db.raw(`
INSERT INTO animal_image
            (id,animal_id,image_url)
VALUES ?           
        `, [insertArray])
   },
}
