//http://www.dpriver.com/pp/sqlformat.htm
export default {
   add: async (opts, db) => {
      const data = await db.raw(`
INSERT INTO volunteer
            (email,
             name,
             date,
             message)
VALUES      (?,
             ?,
             ?,
             ?) 
        `, [opts.email, opts.name, opts.date, opts.message])
      return data[0]
   }

}
