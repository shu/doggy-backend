export default {
    getByEmail: async (opt, db) => {
        const data = await db.raw(`select * from member where email=? and password=?`, [opt.email, opt.password])
        return data[0][0]
    }
}