import knex from 'knex'
import { db } from '../config/index.js';

const configOptions = {
  client: 'mysql',
  connection: {
    host: db.host,
    user: db.user,
    password: db.password,
    database: db.name,
  },
}
export const initDB = async () => {
  await knex(configOptions)
    .raw('SELECT 1').then(() => {
      console.log('Server db connection successful')
      // Success / boot rest of app
    }).catch(err => {
      // Failure / timeout
      throw err
    });
}
export default knex(configOptions)


