const jwt = require('express-jwt')

// JWT middleware
const auth = jwt({
  secret: 'dummy',
}).unless({
  path: '/api/auth/login',
})

module.exports = { auth }
