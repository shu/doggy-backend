import multer from 'multer';

const storage = multer.diskStorage({
  destination: (req, image, cb) => {
    cb(null, `./public/images/animals`);
  },

  filename: (req, image, cb) => {
    try {
      cb(null, image.originalname);
    } catch (err) {
      cb(err);
    }
  },
});
const limits = { fileSize: 100000000 };

// Multer is a node.js middleware for handling multipart/form-data
export const uploadImage = multer({
  storage,
  limits,
}).single("image");

export const uploadMultiImage = multer({
  storage,
  limits,
}).array("images", 10);

