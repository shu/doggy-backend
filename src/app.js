import createError from 'http-errors';
import express, { json, urlencoded } from 'express';
import path, { join } from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import indexRouter from './routes/index.js';
import { initDB } from './server/db.js';
import { port } from './config/index.js';
import { fileURLToPath } from 'url';
import swaggerJsdoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';
import swaggerDocument from './swagger/index.js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const app = express();
import celebrate from 'celebrate';
const { isCelebrateError } = celebrate;


app.use(logger('dev'));
app.use(json());
app.use(urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(join(__dirname, '../public')));

//swagger
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use('/', indexRouter);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {

  if (isCelebrateError(err)) {
    return res.send({
      statusCode: 400,
      message: err.message
    });
  }

  // render the error page
  res.status(err.status || 500);
  return res.send({
    message: err.message
  });
});

async function startServer () {
  try {
    await initDB()
    app
      .listen(port, () => {
        console.info(`server running on port : ${port}`);
      })
      .on('error', (e) =>
        console.error(e));

    console.log('Web AP Service Server is ready.')

  } catch (e) {

  }
}
startServer()


export default app;
