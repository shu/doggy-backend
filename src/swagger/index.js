// * https://swagger.io/specification/
export default
    {
        "openapi": "3.0.0",
        "info": {
            "title": "DOGGY SHELTER",
            "version": "1.0.0",
            "description": "Node server for DOGGY SHELTER"
        },
        paths: {
            "/animal/list": {
                "get": {
                    "tags": [
                        "animal"
                    ],
                    "description": "get animal list",
                    "responses": {
                        "200": {
                            "description": "success",
                        },
                        "400": {
                            "description": "error"
                        }
                    }
                }
            },
            "/animal/one": {
                "get": {
                    "tags": [
                        "animal"
                    ],
                    "description": "get animal by id",
                    "parameters": [
                        {
                            "name": "id",
                            "in": "query",
                            "description": "id",
                            "required": true,
                            "schema": {
                                "type": "number",
                                "example": "1"
                            }
                        }
                    ],
                    "responses": {
                        "200": {
                            "description": "success",
                        },
                        "400": {
                            "description": "error"
                        }
                    }
                }
            },
            "/volunteer/submit": {
                "post": {
                    "tags": [
                        "volunteer"
                    ],
                    "description": "add volunteer",
                    "requestBody": {
                        "content": {
                            "application/x-www-form-urlencoded": {
                                "schema": {
                                    "properties": {
                                        "name": {
                                            "description": "name",
                                            "type": "string",
                                            "example": "test"
                                        },
                                        "date": {
                                            "description": "date",
                                            "type": "string",
                                            "example": "2020-01-29T00:00:00+08:00"
                                        },
                                        "email": {
                                            "description": "email",
                                            "type": "string",
                                            "example": "test@gmail.com"
                                        },
                                        "message": {
                                            "description": "message",
                                            "type": "string",
                                            "example": "test11111"
                                        }
                                    },
                                    "required": [
                                        "name",
                                        "date",
                                        "email",
                                        "message"
                                    ],
                                    "type": "object"
                                }
                            }
                        }
                    },
                    "responses": {
                        "200": {
                            "description": "success",
                        },
                        "400": {
                            "description": "error"
                        }
                    }
                }
            },
            "/animal/addImages": {
                "post": {
                    "tags": [
                        "animal"
                    ],
                    "requestBody": {
                        "required": true,
                        "content": {
                            "multipart/form-data": {
                                "schema": {
                                    "required": [
                                        "id",
                                        "images"
                                    ],
                                    "properties": {
                                        "id": {
                                            "description": "id",
                                            "type": "string",
                                            "example": "1"
                                        },
                                        "images": {
                                            "description": "images",
                                            "type": "array",
                                            "items": {
                                                "type": "string",
                                                "format": "binary"
                                            }
                                        }
                                    },
                                    "type": "object"
                                }
                            }
                        }
                    },
                    "responses": {
                        "200": {
                            "description": "success",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "properties": {
                                            "msg": {
                                                "description": "訊息",
                                                "type": "string",
                                                "example": "File is add."
                                            },
                                            "data": {
                                                "description": "資料",
                                                "type": "object",
                                                "properties": {
                                                    "path": {
                                                        "description": "圖片相對路徑",
                                                        "type": "string",
                                                        "example": "/images/animals/2-1.png"
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        "400": {
                            "description": "error",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "properties": {
                                            "msg": {
                                                "description": "訊息",
                                                "type": "string",
                                                "example": "File is required"
                                            },
                                            "code": {
                                                "description": "code",
                                                "type": "number",
                                                "example": 1005
                                            },
                                            "statusCode": {
                                                "description": "statusCode",
                                                "type": "number",
                                                "example": 400
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
        }
    }