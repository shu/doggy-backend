import volunteerSql from '~api/database/volunteer/index.js';
import db from '~api/server/db.js';

export default {
    submit: async (opt) => {
        await volunteerSql.add(opt, db)
    }
}