import animalSql from '~api/database/animal/index.js';
import db from '~api/server/db.js';
import fs from 'fs';

export default {
    getList: async (opt) => {
        const animalData = await animalSql.getAll(opt, db)
        return { animalList: animalData }
    },
    getOne: async (opt) => {
        const animalData = await animalSql.getOneById(opt, db)
        animalData.imageUrl = animalData.imageUrl.split(',')

        return { animal: animalData }
    },
    add: async (opt, file) => {
        await db.transaction(async trx => {
            const animalId = await animalSql.add(opt, trx)
            const animalImageId = await animalSql.addImage(animalId, trx)
            const type = file.originalname.split(".").pop()
            const fileName = `${animalId}-${animalImageId}.${type}`
            const newPath = `${file.destination}/${fileName}`
            fs.renameSync(file.path, newPath);
            const imageUrl = `${process.env.DOMAIN}/images/animals/${fileName}`
            await animalSql.updateImageById(imageUrl, animalImageId, trx)
        })
    },
    addImages: async (opt, files) => {
        await db.transaction(async trx => {
            const lastId = await animalSql.getLastImageId(trx)
            const animalId = opt.id
            const insertArray = []
            files.forEach((element, index) => {
                const animalImageId = lastId + index + 1
                const type = element.originalname.split(".").pop()
                const fileName = `${animalId}-${animalImageId}.${type}`
                const newPath = `${element.destination}/${fileName}`
                fs.rename(element.path, newPath, (err) => {
                    if (err) throw err;
                });
                const imageUrl = `${process.env.DOMAIN}/images/animals/${fileName}`
                insertArray.push([animalImageId, animalId, imageUrl])
            });
            await animalSql.addImages(insertArray, trx)

        })
    }
}
