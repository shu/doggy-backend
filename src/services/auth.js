import memberSql from '~api/database/member/index.js';
import { jwtSecret } from '~api/config/index.js';
import jwt from 'jsonwebtoken';
import db from '~api/server/db.js';

export default {
    login: async (opt) => {
        const userData = await memberSql.getByEmail(opt, db)
        if (!userData) {
            throw new Error('User not registered');
        }
        const token = createToken(userData)
        return { token, user: userData }
    }
}

const createToken = (user) => {
    const today = new Date();
    const exp = new Date(today);
    exp.setDate(today.getDate() + 60);

    /**
     * A JWT means JSON Web Token, so basically it's a json that is _hashed_ into a string
     * The cool thing is that you can add custom properties a.k.a metadata
     * Here we are adding the userId, role and name
     * Beware that the metadata is public and can be decoded without _the secret_
     * but the client cannot craft a JWT to fake a userId
     * because it doesn't have _the secret_ to sign it
     * more information here: https://softwareontheroad.com/you-dont-need-passport
     */

    return jwt.sign(
        {
            id: user.id, // We are gonna use this in the middleware 'isAuth'
            name: user.name,
            exp: exp.getTime() / 1000,
        },
        jwtSecret
    )
}