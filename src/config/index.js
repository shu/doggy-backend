import dotenv from 'dotenv';

// Set the NODE_ENV to 'development' by default
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const envFound = dotenv.config();
if (envFound.error) {
  // This error should crash whole process

  throw new Error("⚠️  Couldn't find .env file  ⚠️");
}
export const port = process.env.PORT;
export const db = {
  name: process.env.DB_NAME || '',
  host: process.env.DB_HOST || '',
  //port: process.env.DB_PORT || '',
  user: process.env.DB_USER || '',
  password: process.env.DB_PASS || '',
};
export const jwtSecret = process.env.JWT_SECRET;
