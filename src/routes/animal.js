
import { Router } from 'express';
import animalService from '../services/animal.js';
import { celebrate, Joi } from 'celebrate';
import { uploadImage, uploadMultiImage } from '~api/utils/image.js';
import multer from 'multer';

const router = Router();
router.get(
    '/list',
    async (req, res, next) => {
        try {
            const { animalList } = await animalService.getList()
            return res.json({ animalList }).status(200);
        } catch (e) {
            return next(e);
        }
    },
);
router.get(
    '/one',
    celebrate({
        query: Joi.object({
            id: Joi.number().required(),
        })
    }),
    async (req, res, next) => {
        try {
            const { animal } = await animalService.getOne(req.query)
            return res.json({ animal }).status(200);
        } catch (e) {
            return next(e);
        }
    },
);
router.post(
    '/add',
    async (req, res, next) => {
        uploadImage(req, res, function (err) {
            if (err instanceof multer.MulterError) {
                throw err
                // A Multer error occurred when uploading.
            } else if (err) {
                throw err
                // An unknown error occurred when uploading.
            }
            next()
            // Everything went fine.
        })
    },
    celebrate({
        body: Joi.object({
            description: Joi.string().required(),
            status: Joi.string().required(),
            name: Joi.string().required(),
        }),
    }),
    async (req, res, next) => {
        try {
            await animalService.add(req.body, req.file)
            return res.json().status(200);
        } catch (e) {
            return next(e);
        }
    },
);
router.post(
    '/addImages',
    async (req, res, next) => {
        uploadMultiImage(req, res, function (err) {
            if (err instanceof multer.MulterError) {
                throw err
                // A Multer error occurred when uploading.
            } else if (err) {
                throw err
                // An unknown error occurred when uploading.
            }
            next()
            // Everything went fine.
        })
    },
    celebrate({
        body: Joi.object({
            id: Joi.string().required(),

        }),
    }),
    async (req, res, next) => {
        try {
            await animalService.addImages(req.body, req.files)
            return res.json().status(200);
        } catch (e) {
            return next(e);
        }
    },
);
export default router;
