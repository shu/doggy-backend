
import { Router } from 'express';
import authService from '../services/auth.js';
import { celebrate, Joi } from 'celebrate';
//import { Logger } from 'winston';

const router = Router();
router.post(
    '/login',
    celebrate({
        body: Joi.object({
            email: Joi.string().required(),
            password: Joi.string().required(),
        }),
    }),
    async (req, res, next) => {
        try {
            const { user, token } = await authService.login(req.body)
            return res.json({ user, token }).status(200);
        } catch (e) {
            return next(e);
        }
    },
);
export default router;
