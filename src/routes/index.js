import { Router } from 'express';
import auth from './auth.js';
import animal from './animal.js';
import volunteer from './volunteer.js';

const router = Router();
router.use('/auth', auth);
router.use('/animal', animal);
router.use('/volunteer', volunteer);

export default router;
