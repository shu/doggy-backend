import { Router } from 'express';
import volunteerService from '../services/volunteer.js';
import { celebrate, Joi } from 'celebrate';

const router = Router();
router.post(
    '/submit',
    celebrate({
        body: Joi.object({
            email: Joi.string().email().required(),
            date: Joi.date().iso().required(),
            name: Joi.string().required(),
            message: Joi.string().required()
        }),
    }),
    async (req, res, next) => {
        try {
            await volunteerService.submit(req.body)
            return res.json().status(200);
        } catch (e) {
            return next(e);
        }
    },
);
export default router;